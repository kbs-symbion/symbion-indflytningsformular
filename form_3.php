<form name="signupform" method="post" action="">

	<section class="form-section" id="info-personal">

		<h4>Dine personlige informationer</h4>
		 <div class="form-row">
			<div class="form-group col-md-6">
				<label for="navn1">Navn</label>
				<input type="text" class="form-control" id="navn1" name="navn1" required>
			</div>

			<div class="form-group col-md-6">
				<label for="fakturamail">Email</label>
			 	<input type="email" class="form-control" id="fakturamail" name="fakturamail" required>  
			</div>
		 </div>
	      
	  	<div class="form-row">

	    <div class="form-group col-md-6">
	    	<label for="fakturatelefon">Telefonnummer</label>
	    	<input type="text" class="form-control" id="fakturatelefon" name="fakturatelefon" required>
	    </div>

	    <div class="form-group col-md-6 hide-virtuelt">
	        <label for="inputplicens">Registreringsnummer på køretøj</label>
	        <input type="text" id="inputplicens" name="inputplicens" class="form-control">
	        <small class="form-text text-muted">
	          Ønsker du at parkere på vores parkeringsplads, kan du få en gratis p-licens ved at indtaste dit registreringsnummer her.
	        </small>
	    </div>

	  </div>

	  <div class="form-row">
	  	

	  	<div class="form-group col-md-6 hide-virtuelt hide-creators hide-univate">
		    	<label for="kode1">Ønsket pinkode til print og adgang til bygning</label>
				<input type="text" maxlength="4" name="kode1" value="" class="form-control" id="kode1" placeholder="Skal være 4 cifre" required>
				<small class="form-text text-muted">
			  	Du udleveres en personlig brik, som anvendes til printsystem og giver adgang til bygningen udenfor åbningstid.
				</small>
	  	</div>

	  	
	  </div> 

	  <div class="form-row">
	    <div class="form-group col-md-6 hide-virtuelt">
		    <label for="inputPassword">Ønsket adgangskode</label>
		    <input type="password" id="inputPassword" name="inputPassword" class="form-control" required>
		    <small class="form-text text-muted">
		      Benyttes til login i vores intranet - Mit Symbion. Din adgangskode skal være mindst 8 cifre.
		    </small>
		</div>

		    <div class="form-group col-md-6 hide-virtuelt">
			    <label for="inputPasswordverify">Gentag adgangskode</label>
			    <input type="password" id="inputPasswordverify" name="inputPasswordverify" class="form-control" required>
			    
			</div>
	    
	  </div>
	  <div class="form-row">

	  	<div class="form-group col-md-6 show-symbion hide-virtuelt"><label class="control-label">Betaling af frokost</label><div class="form-check"><label class="form-check-label"><input type="radio" class="form-check-input" name="frokost_1" value="Frokost skal faktureres til virksomheden">Frokost skal faktureres til virksomheden</label></div><div class="form-check"><label class="form-check-label"><input type="radio" class="form-check-input" name="frokost_1" value="Frokost skal betales af medarbejder">Frokost betales særskilt med kreditkort</label></div></div>
	  </div>

	  <div class="form-row show-kontor">
	  	<div class="form-group col-md-6">
	  		<label class="control-label">Dine funktioner i virksomheden</label>
	  		<select class="form-control select2" multiple="multiple" name="funktioner_1">
	  			<option value="4" selected="selected">Administrator af medarbejderdata</option>
	  			<option value="2" selected="selected">Direktør</option>
	  			<option value="1" selected="selected">IT ansvarlig</option>
	  			<option value="3" selected="selected">Regnskab, samt modtager af faktura</option>
	  		</select>
	  		<small>Vælg flere med CTRL/CMD knappen</small>
	  	</div>
	  </div>
	  
	</section>

	<div class="show-kontor">
		<section class="form-section" id="info-medarbejdere">
			<h4>Opret dine medarbejdere</h4>

			<div class="forklaring">
				<p>Få dine medarbejdere oprettet i vores system, ved at tilføje dem nedenfor.</p>
				<p>Du har altid mulighed for at oprette flere medarbejdere senere via Mit Symbion.</p>

			</div>

			<div id="employeelist">

			</div>

			<button id="addemployee" type="button" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Tilføj medarbejder</button>
			<button style="display:none;" id="removeemployee" type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Slet medarbejder</button>


		</section>
  	</div>
 
	
  <div class="alert alert-danger hidden" id="validationerror" role="alert" style="display:none">
    <strong>Fejl i indsendelse af formular.</strong> Check at du har udfyldt felterne korrekt.
  </div>

  <button type="submit" class="btn btn-primary">Næste</button>
</form>