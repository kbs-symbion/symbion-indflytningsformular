

<form name="signupform" method="post" action="">

	

	
	<section class="form-section" id="info-service">
	<h4>Serviceydelser</h4>

		<div class="form-row">
			

			<div class="form-group col-md-6 show-kontor">
				<label class="control-label">Ønsket IT-setup ved indflytning</label>
				<div class="form-check">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="it-setup" value="standard" checked>Eget VLAN (standard)
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="it-setup" value="ip">Dedikeret IP-adresse og egen hardware
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="it-setup" value="custom">Custom (egne servere, firewall mv.)</label>
				</div>
				<div class="mt-2 more-info" >
					<a href="#" data-toggle="modal" data-target="#itsetupinfo">Information om IT-setup</a>
				</div>

			</div>

			<div class="form-group col-md-6">
				<div class="form-group">
				    <label class="control-label">Ønskes telefonpasning ved indflytning?</label>
				    <div class="form-check">
				    	<label class="form-check-label">
				    		<input type="checkbox" class="form-check-input" name="telefonpasning[]" value="telefonipasning">Telefonpasning ønskes
				    	</label>
				    </div>
				    <div class="mt-2 more-info" >
				    	<a href="#" data-toggle="modal" data-target="#telefonisetupinfo">Information om Telefonpasning</a>
				    </div>
				</div>
			</div>
				
		</div>

		<div class="form-row">
			<div class="form-group col-md-6 show-virtuelt">
				<div class="form-group">
				    <label class="control-label">Ønskes post åbnet, scannet og sendt via email?</label>
				    <div class="form-check">
				    	<label class="form-check-label">
				    		<input type="checkbox" class="form-check-input" name="postscan" value="postscan">Ja tak. Jeg ønsker at min post til åbnes, scannes og sendes til mig via email.
				    	</label>
				    	<small class="form-text text-muted">
				    		Servicen håndteres i fortrolighed af vores reception.
				    	</small>
				    </div>
				</div>
			</div>
    </div>

    <div class="form-row">
      <div class="form-group col-md-6 show-virtuelt">
        <div class="form-group">
            <label class="control-label">Angiv ønsket password, såfremt der ønskes adgang til at booke mødelokaler på timebasis i Symbion Konferencecenter (valfrit)</label>
            
          
            <input type="password" id="inputKCPassword" name="inputKCPassword" class="form-control">
            <small class="form-text text-muted">
                Se lokaler, betingelser og priser på <a href="https://symbionkonferencecenter.dk" target="_blank">SymbionKonferencecenter.dk</a>
            </small>
        </div>
      </div>
		</div>

    <div class="form-row">
      <div class="form-group col-md-6 show-kontor">
        <div class="form-group">
            <label class="control-label">Antal møbelsæt ønsket ved indflytning</label>
            
          
            <select class="form-control" name="moebelsaet"><option value="0">Ingen møbelsæt</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option></select>

            <small class="form-text text-muted">
                Et møbelsæt koster kr. 165 pr. mdr. ekskl. moms og består af et hvidt hæve-sænke-bord (120 x 80 cm) skrivebordsstol, papirkurv og lille reol.
            </small>
        </div>
      </div>
    </div>


	</section>

 

  <div class="alert alert-danger hidden" id="validationerror" role="alert" style="display:none">
    <strong>Fejl i indsendelse af formular.</strong> Check at du har udfyldt felterne korrekt.
  </div>

  <button type="submit" class="btn btn-primary">Næste</button>

  <div id="telefonisetupinfo" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Information om telefoni</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        	<p>Benedicte Jørgensen er ansvarlig for aftalerne på telefonpasning og kan besvare yderligere spørgsmål (<a href="mailto:bj@symbion.dk" target="_blank">bj@symbion.dk</a>).</p>

        	<h6>Priser på telefonpasning</h6>
        	<p>Prisen på telefonpasning afhænger af antallet af opkald.</p>

        	<table class="table">
        	  <thead>
        	    <tr>
        	      <th scope="col">Antal opkald</th>
        	      <th scope="col">Pris pr. måned</th>
        	    </tr>
        	  </thead>
        	  <tbody>
        	    <tr>
        	      <th scope="row">0-25</th>
        	      <td>425 kr.</td>
        	    </tr>
        	    <tr>
        	      <th scope="row">25-50</th>
        	      <td>650 kr.</td>
        	    </tr>
        	    <tr>
        	      <th scope="row">50-100</th>
        	      <td>900 kr.</td>
        	    </tr>
        	    <tr>
        	      <th scope="row">100+</th>
        	      <td>1500 kr.</td>
        	    </tr>
        	  </tbody>
        	</table>
        	<p><em>Priser er vist eksklusiv moms. Der tages forbehold for fejl og prisændringer.</em></p>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Luk</button>
        </div>
      </div>
    </div>
  </div>

  <div id="itsetupinfo" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Information om IT-setup</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Båndbredden på den kablede forbindelse er 1 Gbit/s.</p>
        	<h6>Eget VLAN</h6>
        	<p>Standardløsning som anvendes af de fleste af vores virksomheder.</p>
        	<p>Dit lokale er tilkoblet vores højhastighedsforbindelse under et virtuelt privat netværk (VLAN). Kun udstyr tilsluttet i jeres lokaler kan "se hinanden".</p>
        	<p>Netværket har udaftil en fælles IP-adresse med resten af huset, og er beskyttet af Symbions egen firewall.</p>

        	<h6>Dedikeret IP-adresse og egen hardware</h6>
        	<p>Din virksomhed får en dedikeret IP-adresse og du har mulighed for at opsætte dit netværksudstyr. Herunder router og firewall.</p>

        	<h6>Custom</h6>
        	<p>Vælg denne løsning, hvis du har særlige behov i forbindelse med dit IT og netværk.</p>
        	<p>Herunder mulighed for opsætning af egen server, firewall etc. der skal hostes i Symbions serverrum.</p>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Luk</button>
        </div>
      </div>
    </div>
  </div>


</form>