<?php 
$form_page = $_GET['page'];
if(is_null($form_page)){
	$form_page = 1;
}
$product_type_string = $_GET['type'];
if(is_null($product_type_string)){
	$product_type_string = 'show-all';
}

$location = $_GET['location'];
if(is_null($location)){
	$location = 'all';
}

$url_append = '&type='.$product_type_string.'&location='.$location;

$product_array = explode(',', $product_type_string);
$product_list = implode(' ', $product_array);


?>

<!DOCTYPE html>
<html>
<head>
	<title>Indflytningsformular</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
<link rel="stylesheet" type="text/css" href="plugins/fastselect/fastselect.min.css">

<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha18/css/tempusdominus-bootstrap-4.min.css" />
<link rel="stylesheet" type="text/css" href="css/style.css">



</head>
<body class="<?php echo $product_list; echo ' '.$location; ?>">

	<!-- !Test-nav-bar - delete on live site! -->

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand" href="#">Produkt:</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#typeselector" aria-controls="typeselector" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="typeselector">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item show-all">
	        <a class="nav-link" href="?type=show-all&location=<?php echo $location; ?>">Alle</a>
	      </li>
	      <li class="nav-item kontor">
	        <a class="nav-link" href="?type=kontor&location=<?php echo $location; ?>">Kontor</a>
	      </li>
	      <li class="nav-item kontorplads">
	        <a class="nav-link" href="?type=kontorplads&location=<?php echo $location; ?>">Kontorplads</a>
	      </li>
	      <li class="nav-item membership">
	        <a class="nav-link" href="?type=membership&location=<?php echo $location; ?>">Membership</a>
	      </li>
	      <li class="nav-item virtuelt">
	        <a class="nav-link" href="?type=virtuelt&location=<?php echo $location; ?>">Virtuelt Kontor</a>
	      </li>
	    </ul>
	    <span class="navbar-text">

	    </span>
	  </div>
	</nav> 

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand" href="#">Lokation:</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#locationselector" aria-controls="locationselector" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="locationselector">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item all">
	        <a class="nav-link" href="?type=<?php echo $product_type_string; ?>&location=all">Alle</a>
	      </li>
	      <li class="nav-item symbion">
	        <a class="nav-link" href="?type=<?php echo $product_type_string; ?>&location=symbion">Symbion</a>
	      </li>
	      <li class="nav-item creators">
	        <a class="nav-link" href="?type=<?php echo $product_type_string; ?>&location=creators">Creators</a>
	      </li>
	      <li class="nav-item univate">
	        <a class="nav-link" href="?type=<?php echo $product_type_string; ?>&location=univate">Univate</a>
	      </li>
	    </ul>
	    <span class="navbar-text">

	    </span>
	  </div>
	</nav>


	<!-- /nav test bar -->


	<div class="container">
		<header>
			<div class="row">
				<div class="col">
					<img width="150" src="https://symbion.dk/wp-content/uploads/symbion-primary-logo.png" alt="Symbion">
				</div>
			</div>		
			
			<div class="row">
				<div class="col-lg-9 mt-2 mb-3">
				<h1>Indflytningsformular</h1>
				<p>Når vi har oplysningerne fra denne formular kan vi oprette alle opgaverne på jeres virksomhed, så I kommer godt i gang som lejer i Symbion.</p>
				
				</div>
			</div>
		</header>
		
		<div class="row">
			<div class="col-lg-9">
				<ul class="form-progress clearfix">
					<li id="form-1"><a href="?page=1<?php echo $url_append; ?>">Informationer</a></li>
					<li id="form-2"><a href="?page=2<?php echo $url_append; ?>">Serviceydelser</a></li>
					<li id="form-3"><a href="?page=3<?php echo $url_append; ?>">Medarbejdere</a></li>
					<li id="form-4"><a href="?page=4<?php echo $url_append; ?>">Virksomheds-beskrivelse</a></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-9">
				
				<?php require_once 'form_'. $form_page .'.php'; ?>
				
			</div>

		</div>

		<footer class="mt-4">
			<hr class="border border-secondary">
			<div class="row">

				<div class="col-md-3 col-6">
					<address>

						<dl>
							<dt>Symbion A/S</dt>
							<dd>Fruebjergvej 3</dd>
							<dd>2100 København Ø</dd>
							<dd>Danmark</dd>
							<dd><strong>CVR:</strong> 10369703</dd>
						</dl>

						
					</address>
				</div>
				<div class="col-md-3 col-6">
					<dl>
						<dt>Kontakt</dt>
						<dd><a href="mailto:info@symbion.dk" target="_blank">info@symbion.dk</a></dd>
						<dd><strong>Tlf:</strong> 39179999</dd>
					</dl>
				</div>
			</div>
			
		</footer>
		
	</div>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha18/js/tempusdominus-bootstrap-4.min.js"></script>
<script type="text/javascript" src="plugins/fastselect/fastselect.standalone.min.js"></script>


<script type="text/javascript">
	function showcvrkund(v) {
			if (v==true)
			{
				$("#nocvr").show("fast")
				$("#tempkundeshow").show("fast")
				$("#inputCVR").attr("disabled", true)
				
			} else {
				$("#nocvr").hide("fast")
				$("#tempkundeshow").hide("fast")
				$("#inputCVR").attr("disabled", false)
			}
		}

	// Highlight active step
	$('#form-<?php echo $form_page; ?>').addClass('active');

	// Test-navbar - remove on live site
	$('.navbar .<?php echo $product_type_string; ?>').addClass('active');
	$('.navbar .<?php echo $location; ?>').addClass('active');

	// Datepicker
	$(function () {
            $('#indflytningsdato').datetimepicker({
                format: 'DD/MM/YYYY', 
                locale: 'da',
                daysOfWeekDisabled: [0, 6], // sat,sun disabled
                stepping: 30, // 30min. stepping
                useCurrent: false,
                minDate: moment('04/05/2018', 'DD/MM/YYYY').add(1, 'day')
            });
            $('#indflytningstid').datetimepicker({
                format: 'HH:mm', 
                stepping: 30 // 30min. stepping
            });
    	});
	$( document ).ready(function() {
	    $('.tagselect').fastselect({
	    	elementClass: 'fstElement col-md-12 form-control',
	    	placeholder: 'Vælg tags',
	    });
	});

	// Medarbejder builder template
	$(document).ready(function(){
		

		var num = 2; // Default starting number

	    $("#addemployee").click(function(){

	    	var medarbejderhtml = '<div class="employee" id="employee_' + num + '"><h5>Medarbejder ' + num + '</h5><div class="form-row">';
	    	
	    	//Fields
	    	medarbejderhtml += '<div class="form-group col-md-6"><label class="control-label">Navn</label><input type="text" name="name_' + num + '" placeholder="" class="form-control" /></div>';
	    	
	    	medarbejderhtml += '<div class="form-group col-md-6"><label class="control-label">Email</label><input type="email" name="email_' + num + '" placeholder="" class="form-control" /></div>';

	    	medarbejderhtml += '<div class="form-group col-md-6"><label class="control-label">Telefon</label><input type="text" name="phone_' + num + '" placeholder="" class="form-control" /></div>';

	    	medarbejderhtml += '<div class="form-group col-md-6"><label class="control-label">Ønsket pinkode til print og adgang til bygning</label><input type="text" name="kode_' + num + '" maxlength="4" placeholder="Skal være 4 cifre" class="form-control" required/></div>';

	    	medarbejderhtml += '<div class="form-group col-md-6"><label for="inputplicens_' + num + '">Registreringsnummer på køretøj</label><input type="text" id="inputplicens' + num + '" name="inputplicens' + num + '" class="form-control"></div>';

	    	medarbejderhtml += '<div class="form-group col-md-6 show-symbion"><label class="control-label">Betaling af frokost</label><div class="form-check"><label class="form-check-label"><input type="radio" class="form-check-input" name="frokost_' + num + '" value="Frokost skal faktureres til virksomheden">Frokost skal faktureres til virksomheden</label></div><div class="form-check"><label class="form-check-label"><input type="radio" class="form-check-input" name="frokost_' + num + '" value="Frokost skal betales af medarbejder" checked>Frokost skal betales af medarbejder</label></div></div>';

	    	medarbejderhtml += '<div class="form-group col-md-6"><label class="control-label">Medarbejderfunktion (valgfri)</label><select class="form-control select2" multiple="multiple" name="funktioner_' + num + '"><option value="0">Standard / Ingen funktion</option><option value="4">Administrator af medarbejderdata</option><option value="2">Direktør</option><option value="1">IT ansvarlig</option><option value="3">Regnskab, samt modtager af faktura</option></select><small>Vælg flere med CTRL/CMD knappen</div>';

	    	// /Fields
	    	medarbejderhtml += '</div></div>';

	        $("#employeelist").append(medarbejderhtml);
	        num++;
	        $('#removeemployee').show();
	    });

	    $("#removeemployee").click(function(){
	    	$('#employeelist .employee').last().remove();
	    	num--;
	    	if (num==2) {
	    		$('#removeemployee').hide();
	    	}
	    });
	}); // End: Medarbejder builder template

	
	
</script>


</body>
</html>