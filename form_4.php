

<form name="signupform" method="post" action="">

	

	
	<section class="form-section" id="info-description">
	<h4>Beskrivelse af din virksomhed</h4>

	<div class="forklaring">
		<p>Du er ved at blive del af Danmarks største iværksætter-community bestående af ambitiøse og videntunge iværksættere fordelt på vores tre lokationer; Symbion, Creators Floor og Univate.</p>
		<p>Vi har et stort fokus på at skabe netværk og videndeling internt, vi arrangere løbende faglige og sociale events og får en masse henvendelser fra investorer, større virksomheder og andre potentielle samarbejdspartnere.</p>
		<p>Vi ønsker at du som del af Symbion Community bliver præsenteret for så mange relevante forretningsmuligheder og tilbud som muligt, men det kræver at vi ved en smule mere om hvad din virksomhed laver.</p>

	</div>
		<div class="form-row">
			<div class="form-group col-md-6"><label class="control-label">Angiv virksomhedens overordnede branche</label><select class="form-control" name="branche" id="branche"><option value="">Vælg branche</option><option value="1">ICT</option><option value="2">Life Science</option><option value="3">Hardware</option><option value="4">Knowledge Consulting</option><option value="5">Media, Web &amp; Communication</option><option value=6>Foodtech</option></select>
				<small class="form-text text-muted">
				  Vælg den branche der passer bedst på din virksomhed.
				</small>
			</div>
			<div class="form-group col-md-6">
				<label class="control-label">Tags</label>
				<input type="text" multiple class="tagselect multipleInputDynamic" data-url="data/company_tags.json" id="company_tags" name="company_tags">

				
			</div>
		</div>
		

		<div class="form-row">
			<div class="form-group col-lg-6">
			    <label for="companydescDA">Virksomhedsbeskrivelse på dansk</label>
			    <textarea class="form-control" id="companydescDA" name="companydescDA" maxlength="350" rows="8"></textarea>
			    <small class="form-text text-muted">
			  	Maks. 350 tegn.
				</small>
			 </div>
			 <div class="form-group col-lg-6">
			    <label for="companydescEN">Virksomhedsbeskrivelse på engelsk</label>
			   	<textarea class="form-control" id="companydescEN" name="companydescEN" maxlength="350" rows="8"></textarea>
			    <small class="form-text text-muted">
			  	Maks. 350 tegn.
				</small>
			 </div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="logo">Logo</label>
				<input type="file" class="form-control" name="logo" id="logo">
				<small class="form-text text-muted">
				  Dette logo vil fremgå på vores webside i oversigten over virksomheder.
				</small>
			</div>
		</div>

	</section>

 

  <div class="alert alert-danger hidden" id="validationerror" role="alert" style="display:none">
    <strong>Fejl i indsendelse af formular.</strong> Check at du har udfyldt felterne korrekt.
  </div>

  <button type="submit" class="btn btn-success">Godkend</button>
</form>