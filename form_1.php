<form name="signupform" method="post" action="">

	<input type="hidden" name="datestarted" value="">
	<input type="hidden" name="ContractSystem_contractsid" value="">
	<input type="hidden" name="subjectid" value="">
	<input type="hidden" name="checkcode" value="">
	<input type="hidden" name="box" value="">
	<input type="hidden" name="roomnr" value="">
	<input type="hidden" name="siteid" value="">
	<input type="hidden" name="realindflytningsdato" value="">

	<section class="form-section" id="info-general">
		<h4>Informationer om din virksomhed</h4>
		
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="navn">Virksomhedsnavn</label>
				<input type="text" class="form-control" id="navn" name="navn" required>
				<small class="form-text text-muted">
				  Det fulde navn på din virksomhed.
				</small>
			</div>
			<div class="form-group col-md-6">
				<label for="Virksomhedsnavnetskilt">Vist virksomhedsnavn på skilte og oversigter</label>
				<input type="text" maxlength="100" name="Virksomhedsnavnetskilt" class="form-control validate" id="Virksomhedsnavnetskilt" required>
				<small class="form-text text-muted">
				  Dette navn vil fremgå på vores webside i oversigten over virksomheder.
				</small>
			</div>

		</div>

		<div class="form-row">
			
			<div class="form-group col-md-6">
				<label for="inputCVR">CVR-nummer</label>
				<div class="input-group">
					<input type="text" class="form-control" id="inputCVR" required>
					
				</div>
				<div class="form-check">
				  <input class="form-check-input" onclick="showcvrkund(this.checked)" type="checkbox" value="" id="ingenCVR" name="ingenCVR">
				  <label class="form-check-label" for="ingenCVR">
				    Selskabet er under stiftelse og har ikke et CVR-nummer
				  </label>
				</div>
			</div>

			<div class="form-group col-md-6">
				<label for="EAN">Evt. EAN</label>
				<input type="text" class="form-control" name="EAN" id="EAN">
			</div>
		</div>
		<div class="form-row">

			<div class="form-group col-md-6" id="nocvr" style="display:none">
				<label for="kontonummer">Telefonnummer</label>
				<input type="text" class="form-control" id="kontonummer" name="kontonummer" required>
			</div>

			<div id="tempkundeshow" class="alert alert-info" role="alert" style="display:none">
			  Når din virksomhed ikke har et CVR-nummer, så bedes du angive et telefonnummer, som vi midlertidigt kan anvende som kundenummer.
			</div>
		</div>
		
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="emailadministration">Virksomhedens hoved e-mail</label>
				<input type="email" class="form-control" id="emailadministration" name="emailadministration" required>
			</div>
			<div class="form-group col-md-6">
				<label for="hjemmeside">Webside</label>
				<input type="email" class="form-control" id="hjemmeside" name="hjemmeside" placeholder="https:// ...">
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-6"><label class="control-label">Angiv selskabsform</label><select class="form-control" name="selskabsform" id="selskabsform" required><option value="">Vælg selskabsform</option><option value="Under stiftelse">Under stiftelse</option><option value="Enkeltmandsvirksomhed">Enkeltmandsvirksomhed</option><option value="IVS">IVS</option><option value="ApS">ApS</option><option value="A/S">A/S</option><option value="I/S">I/S</option><option value="K/S">K/S</option><option value="P/S">P/S</option><option value="Fonde og foreninger">Fonde og foreninger</option><option value="A.M.B.A.">A.M.B.A.</option><option value="S.M.B.A.">S.M.B.A.</option><option value="F.M.B.A.">F.M.B.A.</option><option value="Udenlandsk selskabsform">Udenlandsk selskabsform</option></select></div>
		</div>
		
		<div class="form-row">
		  	<div class="form-group col-md-6">
		  		<label for="inputindflytningsdato">Ønsket indflytningsdato</label>
	            <div class="input-group date" id="indflytningsdato" data-target-input="nearest">
	            	
	                <input type="text" name="oensketindflytningsdato" id="oensketindflytningsdato" class="form-control datetimepicker-input" data-target="#indflytningsdato"/>
	                <div class="input-group-append" data-target="#indflytningsdato" data-toggle="datetimepicker">
	                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
	                </div>
	            </div>
	        </div>

	        <div class="form-group col-md-6">
		  		<label for="inputindflytningstidspunkt">Ønsket tidspunkt for indflytning</label>
	            <div class="input-group date" id="indflytningstid" data-target-input="nearest">
	            	
	                <input type="text" name="oensketindflytningstid" id="oensketindflytningstid" class="form-control datetimepicker-input" data-target="#indflytningstid"/>
	                <div class="input-group-append" data-target="#indflytningstid" data-toggle="datetimepicker">
	                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
	                </div>
	            </div>
	        </div>

	  	</div>
		

	</section>

  <div class="alert alert-danger hidden" id="validationerror" role="alert" style="display:none">
    <strong>Fejl i indsendelse af formular.</strong> Check at du har udfyldt felterne korrekt.
  </div>

  <button type="submit" class="btn btn-primary">Næste</button>
</form>